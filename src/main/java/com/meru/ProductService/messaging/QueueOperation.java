package com.meru.ProductService.messaging;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import com.meru.ProductService.service.ProductService;

@Component
public class QueueOperation {

	private final String INVENTORY_CREATE_QUEUE="inventory.CREATE";
	private final String INVENTORY_DELETE_QUEUE="inventory.DELETE";
	private final String ADD_PRODUCTS_TO_ORDER_REQUEST = "order.request.new.SET_PRODUCTS";
	private final String ADD_INVENTORIES_TO_ORDER_REQUEST = "order.request.new.SET_INVENTORIES";
	
	@Autowired
	private ProductService service;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public void createEmptyInventoryForProductId(long productID)
	{
		jmsTemplate.send(INVENTORY_CREATE_QUEUE, new MessageCreator() {
			@Override
			public MapMessage createMessage(Session session) throws JMSException {
				MapMessage msg=session.createMapMessage();
				msg.setLongProperty("productID", productID);
				return msg;
			}
		});
	}
	
	public void deleteInventoryForProductId(long productID)
	{
		jmsTemplate.send(INVENTORY_DELETE_QUEUE,new MessageCreator() {
			
			@Override
			public MapMessage createMessage(Session session) throws JMSException {
				MapMessage msg = session.createMapMessage();
				msg.setLongProperty("productID", productID);
				return msg;
			}
		});	
	}
	
	@JmsListener(destination = ADD_PRODUCTS_TO_ORDER_REQUEST)
	@SendTo(ADD_INVENTORIES_TO_ORDER_REQUEST)
	public String addProductsToOrderRequest(String jsonOrderRequest)
	{
		return this.service.addProductsToOrderRequest(jsonOrderRequest);
	}
}
