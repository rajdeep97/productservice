package com.meru.ProductService.exception;

public class ProductNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3888581711000683737L;
	
	public ProductNotFoundException(String exceptionMessage)
	{
		super(exceptionMessage);
	}

}
