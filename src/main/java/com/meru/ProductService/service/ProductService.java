package com.meru.ProductService.service;

import java.util.List;

import com.meru.ProductService.model.Product;

public interface ProductService {

	public List<Product> getAllProducts();

	public Product getProductById(long id);

	public Product addProduct(Product product);

	public Product updateProduct(long id, Product updatedProduct);

	public void deleteProduct(long id);

	public String addProductsToOrderRequest(String jsonOrderRequest);

}
