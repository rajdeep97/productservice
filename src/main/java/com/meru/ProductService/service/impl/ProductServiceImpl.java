package com.meru.ProductService.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meru.ProductService.external.model.CartItem;
import com.meru.ProductService.external.model.OrderRequest;
import com.meru.ProductService.exception.InvalidRequestException;
import com.meru.ProductService.exception.ProductNotFoundException;
import com.meru.ProductService.messaging.QueueOperation;
import com.meru.ProductService.model.Product;
import com.meru.ProductService.repository.ProductRepository;
import com.meru.ProductService.service.ProductService;
@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepository repository;
	
	@Autowired
	private QueueOperation queueOperation;
	
	@Override
	public List<Product> getAllProducts() {
		return this.repository.findAll();
	}

	@Override
	public Product getProductById(long id) {
		Optional<Product> product = this.repository.findById(id);
		if(product.isPresent())
		{
			return product.get();
		}
		else
		{
			throw new ProductNotFoundException("No Product found with Id : "+id);
		}
	}

	@Override
	public Product addProduct(Product product) {
		Product newProduct = this.repository.save(product);
		this.queueOperation.createEmptyInventoryForProductId(newProduct.getProductID());
		return newProduct;
	}

	@Override
	public Product updateProduct(long id, Product updatedProduct) {
		if(updatedProduct.getProductID() != id)
		{
			throw new InvalidRequestException("ProductID mismatch in URI and body");
		}
		Optional<Product> product = this.repository.findById(id);
		if(product.isPresent())
		{
			return this.repository.save(updatedProduct);
		}
		else
		{
			throw new ProductNotFoundException("No Product found with Id : "+id);
		}
	}

	@Override
	public void deleteProduct(long id) {
		Optional<Product> product = this.repository.findById(id);
		if(product.isPresent())
		{
			queueOperation.deleteInventoryForProductId(id);
			this.repository.deleteById(id);
		}
		else
		{
			throw new ProductNotFoundException("No Product found with Id : "+id);
		}
		
	}

	@Override
	public String addProductsToOrderRequest(String jsonOrderRequest) {
		ObjectMapper om = new ObjectMapper();
		String productsAddedOrder = null;
		try {
			OrderRequest orderRequest = om.readValue(jsonOrderRequest, OrderRequest.class);
			List<CartItem> shoppingCart = orderRequest.getCart().getShoppingCart(); 
			if(shoppingCart.size()==0)
			{
				return jsonOrderRequest;
			}
			List<Product> products = new ArrayList<Product>();
			for(CartItem cartItem : shoppingCart)
			{
				Product product = this.getProductById(cartItem.getProductID());
				products.add(product);
			}
			
			orderRequest.setProducts(products);
			productsAddedOrder = om.writeValueAsString(orderRequest);
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return productsAddedOrder;
	}
}
