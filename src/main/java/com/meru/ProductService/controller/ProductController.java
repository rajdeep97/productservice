package com.meru.ProductService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meru.ProductService.model.Product;
import com.meru.ProductService.service.ProductService;


@RestController
public class ProductController {

	@Autowired
	private ProductService service;
	
	@GetMapping("/products")
	public List<Product> getAllProducts()
	{
		return this.service.getAllProducts();
	}
	
	@GetMapping("/product/{id}")
	public Product getProduct(@PathVariable long id)
	{
		return this.service.getProductById(id);
	}
	
	@PostMapping("/product")
	public ResponseEntity<Product> addProduct(@RequestBody Product product)
	{
		Product newProduct = this.service.addProduct(product);
		return new ResponseEntity<Product>(newProduct,HttpStatus.CREATED);
	}
	
	@PutMapping("/product/{id}")
	public Product updateProduct(@PathVariable long id, @RequestBody Product updatedProduct)
	{
		return this.service.updateProduct(id,updatedProduct);
	}
	
	@DeleteMapping("/product/{id}")
	public void deleteProduct(@PathVariable long id)
	{
		this.service.deleteProduct(id);
	}
}
